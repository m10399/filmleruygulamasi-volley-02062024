package com.example.filmleruygulamasivolley

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.example.filmleruygulamasivolley.databinding.ActivityDetayBinding
import com.squareup.picasso.Picasso

class DetayActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetayBinding
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityDetayBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        val film = intent.getSerializableExtra("filmlerNesne") as Filmler
        val url = "http://kasimadalan.pe.hu/filmler/resimler/${film.film_resim}"
        binding.textViewFilmAdDetay.text = film.film_ad
        binding.textViewFilmYilDetay.setText(film.film_yil.toString())
        binding.textViewFilmYonetmenDetay.text = film.yonetmen.yonetmen_ad

        Picasso.get().load(url).into(binding.imageViewResim)

    }
}