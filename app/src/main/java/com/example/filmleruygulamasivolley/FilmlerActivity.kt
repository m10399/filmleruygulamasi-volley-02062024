package com.example.filmleruygulamasivolley

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.filmleruygulamasivolley.databinding.ActivityFilmlerBinding
import org.json.JSONObject

class FilmlerActivity : AppCompatActivity() {
    private lateinit var binding: ActivityFilmlerBinding
    private lateinit var filmlerListe:ArrayList<Filmler>
    private lateinit var adapter: FilmlerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityFilmlerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        val kategori = intent.getSerializableExtra("kategoriNesne") as Kategoriler

        binding.toolbarFilmlerDetay.title = "Filmler : ${kategori.kategori_ad}"
        setSupportActionBar(binding.toolbarFilmlerDetay)

        binding.filmlerRv.setHasFixedSize(true)
        binding.filmlerRv.layoutManager = StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL)


tumFilmlerByKategoriId(kategori.kategori_id)
    }

    fun tumFilmlerByKategoriId(kategori_id:Int){

        var url = "http://kasimadalan.pe.hu/filmler/filmler_by_kategori_id.php"
        val istek = object :StringRequest(Request.Method.POST,url, { cevap->
            try {
                filmlerListe = ArrayList()
                val jsonObject = JSONObject(cevap)
                val filmler = jsonObject.getJSONArray("filmler")
                for (i in 0 until filmler.length()){
                    val f = filmler.getJSONObject(i)
                    val k = f.getJSONObject("kategori")
                    val kategori = Kategoriler(k.getInt("kategori_id"),
                        k.getString("kategori_ad"))
                    val y = f.getJSONObject("yonetmen")
                    val yonetmen = Yonetmenler(y.getInt("yonetmen_id"),
                        y.getString("yonetmen_ad"))
                    val film = Filmler(f.getInt("film_id"),
                        f.getString("film_ad"),
                        f.getInt("film_yil"),
                        f.getString("film_resim"),
                        kategori,
                        yonetmen
                        )
                    filmlerListe.add(film)
                }
                adapter = FilmlerAdapter(this,filmlerListe)
                binding.filmlerRv.adapter = adapter
            }
            catch (e:Exception){
                e.printStackTrace()
            }




        }, {  }){
            override fun getParams(): MutableMap<String, String>? {
                val params = HashMap<String,String>()
                params["kategori_id"] = kategori_id.toString()
                return params
                return super.getParams()
            }
        }
        Volley.newRequestQueue(this@FilmlerActivity).add(istek)
    }
}