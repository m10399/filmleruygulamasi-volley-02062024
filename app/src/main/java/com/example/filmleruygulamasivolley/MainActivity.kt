package com.example.filmleruygulamasivolley

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.filmleruygulamasivolley.databinding.ActivityMainBinding
import org.json.JSONObject

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var kategoriListe:ArrayList<Kategoriler>
    private lateinit var adapter:KategorilerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
        binding.toolbarMainActivity.title = "Filmler uygulamasi"
        setSupportActionBar(binding.toolbarMainActivity)

        binding.kategoriRv.setHasFixedSize(true)
        binding.kategoriRv.layoutManager = LinearLayoutManager(this)

        tumKategoriler()
    }

    fun tumKategoriler(){
        val url = "http://kasimadalan.pe.hu/filmler/tum_kategoriler.php"
        val istek = StringRequest(Request.Method.GET,url, { cevap->
            try {
                kategoriListe = ArrayList()
                val jsonObject = JSONObject(cevap)
                val kategoriler = jsonObject.getJSONArray("kategoriler")
                for (i in 0 until kategoriler.length()){
                    val k = kategoriler.getJSONObject(i)
                    val kategori = Kategoriler(k.getInt("kategori_id"),
                        k.getString("kategori_ad"))
                    kategoriListe.add(kategori)
                }

                adapter = KategorilerAdapter(this,kategoriListe)
                binding.kategoriRv.adapter = adapter
            }
            catch (e:Exception){
                e.printStackTrace()
            }
        }, {  })
        Volley.newRequestQueue(this@MainActivity).add(istek)

    }
}