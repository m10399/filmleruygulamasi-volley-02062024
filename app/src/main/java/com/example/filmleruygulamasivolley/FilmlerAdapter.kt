package com.example.filmleruygulamasivolley

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso

class FilmlerAdapter(private val mContext: Context, private val filmlerListe:List<Filmler>):RecyclerView.Adapter<FilmlerAdapter.CardTasarimTutucu>() {
    inner class CardTasarimTutucu(tasarim:View):RecyclerView.ViewHolder(tasarim){
        var film_card:CardView
        var imageViewFilmResim:ImageView
        var textViewFilmAd:TextView
        init {
            film_card = tasarim.findViewById(R.id.film_card)
            imageViewFilmResim = tasarim.findViewById(R.id.imageViewFilmResim)
            textViewFilmAd = tasarim.findViewById(R.id.textViewFilmAd)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardTasarimTutucu {
        var tasarim = LayoutInflater.from(mContext).inflate(R.layout.filmler_card_tasarimi,parent,false)
        return CardTasarimTutucu(tasarim)
    }

    override fun getItemCount(): Int {
        return filmlerListe.size
    }

    override fun onBindViewHolder(holder: CardTasarimTutucu, position: Int) {
        val film = filmlerListe.get(position)

        holder.textViewFilmAd.text = film.film_ad
        val url = "http://kasimadalan.pe.hu/filmler/resimler/${film.film_resim}"
        Picasso.get().load(url).into(holder.imageViewFilmResim)
        holder.film_card.setOnClickListener {
            val intent = Intent(mContext,DetayActivity::class.java)
            intent.putExtra("filmlerNesne",film)
            mContext.startActivity(intent)
        }
    }
}